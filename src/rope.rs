use alloc::arc::Arc;
use alloc::borrow::ToOwned;
use alloc::string::{String, ToString};

use core::ops::{Add, Range};
use core::{fmt, str};

use super::{char_to_byte_index, split_at_char_index, Bytes, Chars, Chunks, Lines};

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Rope {
    Node(usize, Arc<Rope>, Arc<Rope>),
    Leaf(Arc<String>),
}

unsafe impl Sync for Rope {}
unsafe impl Send for Rope {}

impl Rope {
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::new();
    /// assert_eq!(&rope, "");
    /// ```
    #[inline]
    pub fn new() -> Self {
        Rope::Leaf(Arc::new(String::new()))
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello, world!");
    /// assert_eq!(rope.len(), 13);
    /// ```
    #[inline(always)]
    pub fn len(&self) -> usize {
        self.chars_len()
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello, world!");
    /// assert_eq!(rope.bytes_len(), 13);
    /// ```
    #[inline]
    pub fn bytes_len(&self) -> usize {
        match self {
            &Rope::Node(len, _, ref right) => len + right.len(),
            &Rope::Leaf(ref string) => string.len(),
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("長さ");
    /// assert_eq!(rope.chars_len(), 2);
    /// ```
    #[inline]
    pub fn chars_len(&self) -> usize {
        match self {
            &Rope::Node(_, ref left, ref right) => left.chars_len() + right.chars_len(),
            &Rope::Leaf(ref string) => string.chars().count(),
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello, world!");
    /// assert!(!rope.is_empty());
    /// ```
    #[inline]
    pub fn is_empty(&self) -> bool {
        match self {
            &Rope::Node(len, _, ref right) => len == 0 && right.is_empty(),
            &Rope::Leaf(ref string) => string.is_empty(),
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let a = Rope::from("Hello");
    /// let b = a.insert_at_byte(5, ", world!");
    /// assert_eq!(b.height(), 2);
    /// ```
    #[inline]
    pub fn height(&self) -> usize {
        match self {
            &Rope::Node(_, ref left, ref right) => 1 + left.height().max(right.height()),
            &Rope::Leaf(_) => 1,
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let a = Rope::from("Hello,\n");
    /// let b = a.insert_at_byte(5, "world!\n");
    /// assert_eq!(b.count_lines(), 2);
    /// ```
    #[inline]
    pub fn count_lines(&self) -> usize {
        match self {
            &Rope::Node(_, ref left, ref right) => left.count_lines() + right.count_lines(),
            &Rope::Leaf(ref string) => {
                let mut count = 0;

                for ch in string.chars() {
                    if ch == '\n' {
                        count += 1;
                    }
                }

                count
            }
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Helloworld!");
    /// assert_eq!(&rope.insert_at_byte(5, ", "), "Hello, world!");
    /// ```
    #[inline]
    pub fn insert_at_byte<T>(&self, index: usize, value: T) -> Rope
    where
        T: Into<Rope>,
    {
        let (left, right) = self.split_at_byte(index);
        left.concat(value).concat(right)
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("長さ");
    /// assert_eq!(&rope.insert(1, ", "), "長, さ");
    /// ```
    #[inline]
    pub fn insert<T>(&self, index: usize, value: T) -> Rope
    where
        T: Into<Rope>,
    {
        let (left, right) = self.split(index);
        left.concat(value).concat(right)
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello world");
    /// assert_eq!(&rope.insert_range_byte(5..6, ", "), "Hello, world");
    /// assert_eq!(&rope.insert_range_byte(0..0, "!"), "!Hello world");
    /// assert_eq!(&rope.insert_range_byte(11..11, "!"), "Hello world!");
    /// ```
    #[inline]
    pub fn insert_range_byte<T>(&self, range: Range<usize>, value: T) -> Rope
    where
        T: Into<Rope>,
    {
        assert!(
            range.start <= range.end,
            "range start most be less than or equal to range end"
        );
        let (left, tmp) = self.split_at_byte(range.start);
        let (_, right) = tmp.split_at_byte(range.end);
        left.concat(value).concat(right)
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("長 さ");
    /// assert_eq!(&rope.insert_range(1..2, ", "), "長, さ");
    /// assert_eq!(&rope.insert_range(0..0, "!"), "!長 さ");
    /// assert_eq!(&rope.insert_range(3..3, "!"), "長 さ!");
    /// ```
    #[inline]
    pub fn insert_range<T>(&self, range: Range<usize>, value: T) -> Rope
    where
        T: Into<Rope>,
    {
        assert!(
            range.start <= range.end,
            "Range start should be less than or equal to Range end."
        );
        let (left, tmp) = self.split(range.start);
        let (_, right) = tmp.split(range.end);
        left.concat(value).concat(right)
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let a = Rope::from("Hello").concat(" world");
    /// let b = a.insert_char_at_byte(5, ',');
    /// let c = b.insert_char_at_byte(12, '!');
    /// assert_eq!(&b, "Hello, world");
    /// assert_eq!(&c, "Hello, world!");
    /// ```
    #[inline]
    pub fn insert_char_at_byte(&self, index: usize, ch: char) -> Rope {
        match self {
            &Rope::Node(len, ref left, ref right) => {
                if index <= len {
                    left.insert_char_at_byte(index, ch).concat(right)
                } else {
                    left.concat(right.insert_char_at_byte(index - len, ch))
                }
            }
            &Rope::Leaf(ref string) => {
                let mut next_string = (&**string).clone();

                if index >= string.len() {
                    next_string.push(ch);
                } else {
                    next_string.insert(index, ch);
                }

                next_string.into()
            }
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let a = Rope::from("長").concat("さ");
    /// let b = a.insert_char(1, ',');
    /// let c = b.insert_char(3, '!');
    /// assert_eq!(&b, "長,さ");
    /// assert_eq!(&c, "長,さ!");
    /// ```
    #[inline]
    pub fn insert_char(&self, index: usize, ch: char) -> Rope {
        match self {
            &Rope::Node(_, ref left, ref right) => {
                let len = left.chars_len();

                if index <= len {
                    left.insert_char(index, ch).concat(right)
                } else {
                    left.concat(right.insert_char(index - len, ch))
                }
            }
            &Rope::Leaf(ref string) => {
                let byte_index = char_to_byte_index(string.as_str(), index);
                let mut next_string = (&**string).clone();

                if byte_index >= next_string.len() {
                    next_string.push(ch);
                } else {
                    next_string.insert(byte_index, ch);
                }

                next_string.into()
            }
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello, world!");
    /// assert_eq!(&rope.remove_range_byte(5..13), "Hello");
    /// ```
    #[inline]
    pub fn remove_range_byte(&self, range: Range<usize>) -> Rope {
        assert!(
            range.end >= range.start,
            "Range end should not be less than Range start."
        );
        let (left, tmp) = self.split_at_byte(range.start);
        let (_, right) = tmp.split_at_byte(range.end - range.start);
        left.concat(right)
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("長 さ");
    /// assert_eq!(&rope.remove_range(1..2), "長さ");
    /// ```
    #[inline]
    pub fn remove_range(&self, range: Range<usize>) -> Rope {
        assert!(
            range.end >= range.start,
            "Range end should not be less than Range start."
        );
        let (left, tmp) = self.split(range.start);
        let (_, right) = tmp.split(range.end - range.start);
        left.concat(right)
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello, world!");
    /// assert_eq!(&rope.remove_at_byte(12), "Hello, world");
    /// assert_eq!(&rope.remove_at_byte(5), "Hello world!");
    /// ```
    #[inline]
    pub fn remove_at_byte(&self, index: usize) -> Rope {
        match self {
            &Rope::Node(len, ref left, ref right) => {
                if index <= len {
                    left.remove_at_byte(index).concat(right)
                } else {
                    left.concat(right.remove_at_byte(index - len))
                }
            }
            &Rope::Leaf(ref string) => if index < string.len() {
                let mut next_string = (&**string).clone();
                next_string.remove(index);
                next_string.into()
            } else {
                self.clone()
            },
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("長さ");
    /// assert_eq!(&rope.remove(0), "さ");
    /// assert_eq!(&rope.remove(1), "長");
    /// ```
    #[inline]
    pub fn remove(&self, index: usize) -> Rope {
        match self {
            &Rope::Node(_, ref left, ref right) => {
                let len = left.chars_len();

                if index <= len {
                    left.remove(index).concat(right)
                } else {
                    left.concat(right.remove(index - len))
                }
            }
            &Rope::Leaf(ref string) => {
                let byte_index = char_to_byte_index(string.as_str(), index);

                if byte_index < string.len() {
                    let mut next_string = (&**string).clone();
                    next_string.remove(byte_index);
                    next_string.into()
                } else {
                    self.clone()
                }
            }
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    ///
    /// let rope = Rope::from("Hello, world!");
    /// let (left, right) = rope.split_at_byte(7);
    /// assert_eq!(&left, "Hello, ");
    /// assert_eq!(&right, "world!");
    ///
    /// let rope = Rope::new();
    /// let (left, right) = rope.split_at_byte(0);
    /// assert_eq!(&left, "");
    /// assert_eq!(&right, "");
    /// ```
    #[inline]
    pub fn split_at_byte(&self, index: usize) -> (Rope, Rope) {
        match self {
            &Rope::Node(len, ref left, ref right) => {
                if index < len {
                    let (a, b) = left.split_at_byte(index);
                    (a, b.concat(right))
                } else if index > len {
                    let (a, b) = right.split_at_byte(index - len);
                    (left.concat(a), b)
                } else {
                    (left.into(), right.into())
                }
            }
            &Rope::Leaf(ref string) => {
                if index >= string.len() {
                    (self.into(), "".into())
                } else {
                    let (left, right) = string.split_at(index);
                    (left.into(), right.into())
                }
            }
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    ///
    /// let rope = Rope::from("長さ");
    /// let (left, right) = rope.split(1);
    /// assert_eq!(&left, "長");
    /// assert_eq!(&right, "さ");
    /// ```
    #[inline]
    pub fn split(&self, index: usize) -> (Rope, Rope) {
        match self {
            &Rope::Node(_, ref left, ref right) => {
                let len = left.chars_len();

                if index < len {
                    let (a, b) = left.split(index);
                    (a, b.concat(right))
                } else if index > len {
                    let (a, b) = right.split(index - len);
                    (left.concat(a), b)
                } else {
                    (left.into(), right.into())
                }
            }
            &Rope::Leaf(ref string) => {
                let (left, right) = split_at_char_index(string.as_str(), index);
                (left.into(), right.into())
            }
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let left = Rope::from("Hello, ");
    /// let right = Rope::from("world!");
    /// assert_eq!(&left.concat(right), "Hello, world!");
    /// ```
    #[inline]
    pub fn concat<T>(&self, value: T) -> Rope
    where
        T: Into<Rope>,
    {
        let rope: Rope = value.into();
        let len = self.bytes_len();

        if len == 0 {
            rope
        } else if rope.is_empty() {
            self.into()
        } else {
            Rope::Node(len, Arc::new(self.clone()), Arc::new(rope))
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello").concat(", world");
    /// println!("{:?}", rope);
    /// assert_eq!(&rope.concat_char('!'), "Hello, world!");
    /// ```
    #[inline]
    pub fn concat_char(&self, ch: char) -> Rope {
        match self {
            &Rope::Node(len, ref left, ref right) => {
                Rope::Node(len, left.clone(), Arc::new(right.concat_char(ch)))
            }
            &Rope::Leaf(ref string) => {
                let mut next_string = (&**string).clone();
                next_string.push(ch);
                next_string.into()
            }
        }
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("長").concat(", さ!");
    /// assert_eq!(rope.char_at(0), Some('長'));
    /// assert_eq!(rope.char_at(1), Some(','));
    /// assert_eq!(rope.char_at(2), Some(' '));
    /// assert_eq!(rope.char_at(3), Some('さ'));
    /// assert_eq!(rope.char_at(4), Some('!'));
    /// ```
    #[inline]
    pub fn char_at(&self, index: usize) -> Option<char> {
        self.chars().nth(index)
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello").concat(", world!");
    /// assert_eq!(rope.chunks().collect::<Vec<&String>>(), ["Hello", ", world!"].to_vec());
    /// ```
    #[inline(always)]
    pub fn chunks(&self) -> Chunks {
        Chunks::from(self)
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello");
    /// assert_eq!(rope.bytes().collect::<Vec<u8>>(), [72, 101, 108, 108, 111].to_vec());
    /// ```
    #[inline(always)]
    pub fn bytes(&self) -> Bytes {
        Bytes::from(self)
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello");
    /// assert_eq!(rope.chars().collect::<Vec<char>>(), ['H', 'e', 'l', 'l', 'o'].to_vec());
    /// ```
    #[inline(always)]
    pub fn chars(&self) -> Chars {
        Chars::from(self)
    }
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello\n world!");
    /// let lines = rope.lines().collect::<Vec<String>>();
    /// assert_eq!(lines.get(0), Some(&String::from("Hello")));
    /// assert_eq!(lines.get(1), Some(&String::from(" world!")));
    /// ```
    #[inline(always)]
    pub fn lines(&self) -> Lines {
        Lines::from(self)
    }
    #[inline(always)]
    pub fn iter(&self) -> Chars {
        self.chars()
    }
}

impl From<Arc<Rope>> for Rope {
    #[inline]
    fn from(arc: Arc<Rope>) -> Rope {
        match Arc::try_unwrap(arc) {
            Ok(rope) => rope,
            Err(arc) => (&*arc).clone(),
        }
    }
}

impl<'a> From<&'a Arc<Rope>> for Rope {
    #[inline(always)]
    fn from(rope: &'a Arc<Rope>) -> Rope {
        (&**rope).clone()
    }
}

impl<'a> From<&'a Rope> for Rope {
    #[inline(always)]
    fn from(rope: &'a Rope) -> Rope {
        rope.clone()
    }
}

impl<'a> From<&'a str> for Rope {
    #[inline(always)]
    fn from(string: &'a str) -> Self {
        Rope::from(string.to_owned())
    }
}
impl<'a> From<&'a String> for Rope {
    #[inline(always)]
    fn from(string: &'a String) -> Self {
        Rope::from(string.clone())
    }
}
impl From<String> for Rope {
    #[inline(always)]
    fn from(string: String) -> Self {
        Rope::Leaf(Arc::new(string))
    }
}

impl Into<String> for Rope {
    #[inline(always)]
    fn into(self) -> String {
        self.to_string()
    }
}

impl<'a> IntoIterator for &'a Rope {
    type Item = char;
    type IntoIter = Chars<'a>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<T> Add<T> for Rope
where
    T: Into<Rope>,
{
    type Output = Rope;
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let a = Rope::from("Hello");
    /// let b = Rope::from(", world!");
    /// assert_eq!(a.clone() + b, "Hello, world!");
    /// assert_eq!(a + ", world!", "Hello, world!");
    /// ```
    #[inline(always)]
    fn add(self, other: T) -> Self::Output {
        self.concat(other)
    }
}

impl<'a, T> Add<T> for &'a Rope
where
    T: Into<Rope>,
{
    type Output = Rope;
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let a = Rope::from("Hello");
    /// let b = Rope::from(", world!");
    /// assert_eq!(&a + &b, "Hello, world!");
    /// assert_eq!(&a + b.clone(), "Hello, world!");
    /// assert_eq!(a.clone() + &b, "Hello, world!");
    /// ```
    #[inline(always)]
    fn add(self, other: T) -> Self::Output {
        self.concat(other)
    }
}

impl Add<char> for Rope {
    type Output = Rope;
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let a = Rope::from("Hello");
    /// assert_eq!(a + '!', "Hello!");
    /// ```
    #[inline(always)]
    fn add(self, ch: char) -> Self::Output {
        self.concat_char(ch)
    }
}

impl<'a> Add<char> for &'a Rope {
    type Output = Rope;
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let a = Rope::from("Hello");
    /// assert_eq!(&a + '!', "Hello!");
    /// ```
    #[inline(always)]
    fn add(self, ch: char) -> Self::Output {
        self.concat_char(ch)
    }
}

impl ToString for Rope {
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello, world!");
    /// assert_eq!(rope.to_string(), "Hello, world!");
    /// ```
    #[inline]
    fn to_string(&self) -> String {
        let mut string = String::new();

        for s in self.chunks() {
            string.push_str(s.as_str());
        }

        string
    }
}

impl fmt::Debug for Rope {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.to_string())
    }
}

impl<'a> PartialEq<&'a str> for Rope {
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello, world!");
    /// assert_eq!(&rope, "Hello, world!");
    /// assert_ne!(&rope, "Hello!");
    /// ```
    #[inline]
    fn eq(&self, other: &&'a str) -> bool {
        let mut other_chars = other.chars();

        for a in self.chars() {
            if let Some(b) = other_chars.next() {
                if a != b {
                    return false;
                } else {
                    continue;
                }
            } else {
                return false;
            }
        }

        return true;
    }
}

impl PartialEq<str> for Rope {
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello, world!");
    /// assert_eq!(rope, "Hello, world!");
    /// assert_ne!(rope, "Hello!");
    /// ```
    #[inline(always)]
    fn eq(&self, other: &str) -> bool {
        self == &other
    }
}

impl PartialEq<String> for Rope {
    /// # Examples
    /// ```
    /// use persistent_rope::Rope;
    /// let rope = Rope::from("Hello, world!");
    /// assert_eq!(&rope, &String::from("Hello, world!"));
    /// assert_ne!(&rope, &String::from("Hello!"));
    /// ```
    #[inline(always)]
    fn eq(&self, other: &String) -> bool {
        self == other.as_str()
    }
}
