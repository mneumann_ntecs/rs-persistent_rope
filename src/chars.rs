use alloc::vec::Vec;

use super::{Chunks, Rope};

pub struct Chars<'a> {
    index: usize,
    current: Option<Vec<char>>,
    chunks: Chunks<'a>,
}

unsafe impl<'a> Sync for Chars<'a> {}
unsafe impl<'a> Send for Chars<'a> {}

impl<'a> From<&'a Rope> for Chars<'a> {
    #[inline]
    fn from(rope: &'a Rope) -> Chars<'a> {
        let mut chunks = Chunks::from(rope);

        Chars {
            index: 0,
            current: chunks.next().map(|s| s.chars().collect()),
            chunks: chunks,
        }
    }
}

impl<'a> Iterator for Chars<'a> {
    type Item = char;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(ref chars) = self.current {
            match chars.get(self.index) {
                Some(ch) => {
                    self.index += 1;
                    return Some(*ch);
                }
                None => {
                    self.index = 0;
                }
            }
        } else {
            return None;
        }

        self.current = self.chunks.next().map(|s| s.chars().collect());
        self.next()
    }
}
