use alloc::string::String;
use alloc::vec::Vec;

use super::Rope;

pub struct Chunks<'a> {
    stack: Vec<&'a Rope>,
    current: Option<&'a String>,
}

unsafe impl<'a> Sync for Chunks<'a> {}
unsafe impl<'a> Send for Chunks<'a> {}

impl<'a> From<&'a Rope> for Chunks<'a> {
    #[inline]
    fn from(rope: &'a Rope) -> Self {
        let mut iter = Chunks {
            stack: Vec::new(),
            current: None,
        };
        iter.load_stack(rope);
        iter
    }
}

impl<'a> Chunks<'a> {
    #[inline]
    fn load_stack(&mut self, mut rope: &'a Rope) {
        loop {
            match rope {
                &Rope::Node(_, ref left, ref right) => {
                    self.stack.push(right);
                    rope = left;
                }
                &Rope::Leaf(ref string) => {
                    self.current = Some(string);
                    break;
                }
            }
        }
    }
}

impl<'a> Iterator for Chunks<'a> {
    type Item = &'a String;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        let result = self.current.take();

        if let Some(rope) = self.stack.pop() {
            self.load_stack(rope);
        }

        result
    }
}
