#![feature(alloc)]
#![no_std]

extern crate alloc;

mod bytes;
mod chars;
mod chunks;
mod lines;
mod rope;
mod utils;

pub use self::bytes::Bytes;
pub use self::chars::Chars;
pub use self::chunks::Chunks;
pub use self::lines::Lines;
pub use self::rope::Rope;
pub use self::utils::{char_to_byte_index, split_at_char_index};
