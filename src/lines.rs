use alloc::string::String;

use super::{Chars, Rope};

pub struct Lines<'a> {
    chars: Chars<'a>,
}

unsafe impl<'a> Sync for Lines<'a> {}
unsafe impl<'a> Send for Lines<'a> {}

impl<'a> From<&'a Rope> for Lines<'a> {
    #[inline]
    fn from(rope: &'a Rope) -> Lines<'a> {
        Lines {
            chars: Chars::from(rope),
        }
    }
}

impl<'a> Iterator for Lines<'a> {
    type Item = String;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        let mut string = String::new();

        while let Some(ch) = self.chars.next() {
            if ch == '\r' {
                if let Some(next_ch) = self.chars.next() {
                    if next_ch == '\n' {
                        continue;
                    } else {
                        string.push(ch);
                        string.push(next_ch);
                    }
                } else {
                    string.push(ch);
                    break;
                }
            } else if ch != '\n' {
                string.push(ch);
            } else {
                break;
            }
        }

        if string.len() != 0 {
            Some(string)
        } else {
            None
        }
    }
}
