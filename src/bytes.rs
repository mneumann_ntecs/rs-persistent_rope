use super::{Chunks, Rope};

pub struct Bytes<'a> {
    index: usize,
    current: Option<&'a [u8]>,
    chunks: Chunks<'a>,
}

unsafe impl<'a> Sync for Bytes<'a> {}
unsafe impl<'a> Send for Bytes<'a> {}

impl<'a> From<&'a Rope> for Bytes<'a> {
    #[inline]
    fn from(rope: &'a Rope) -> Bytes<'a> {
        let mut chunks = Chunks::from(rope);

        Bytes {
            index: 0,
            current: chunks.next().map(|s| s.as_bytes()),
            chunks: chunks,
        }
    }
}

impl<'a> Iterator for Bytes<'a> {
    type Item = u8;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(ref bytes) = self.current {
            match bytes.get(self.index) {
                Some(ch) => {
                    self.index += 1;
                    return Some(*ch);
                }
                None => {
                    self.index = 0;
                }
            }
        } else {
            return None;
        }

        self.current = self.chunks.next().map(|s| s.as_bytes());
        self.next()
    }
}
