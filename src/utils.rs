/// # Examples
/// ```
/// use persistent_rope::split_at_char_index;
///
/// assert_eq!(split_at_char_index("長さ", 1), ("長", "さ"));
/// assert_eq!(split_at_char_index("長さ", 0), ("", "長さ"));
/// assert_eq!(split_at_char_index("長さ", 2), ("長さ", ""));
/// assert_eq!(split_at_char_index("長さ", 3), ("長さ", ""));
/// ```
#[inline]
pub fn split_at_char_index(string: &str, index: usize) -> (&str, &str) {
    string.split_at(char_to_byte_index(string, index))
}
/// # Examples
/// ```
/// use persistent_rope::char_to_byte_index;
/// assert_eq!(char_to_byte_index("長さ", 0), 0);
/// assert_eq!(char_to_byte_index("長さ", 1), 3);
/// assert_eq!(char_to_byte_index("長さ", 2), 6);
/// ```
#[inline]
pub fn char_to_byte_index(string: &str, char_index: usize) -> usize {
    if let Some((byte_index, _)) = string.char_indices().nth(char_index) {
        byte_index
    } else {
        string.len()
    }
}
